
Retrieves the Experiment entity having the specified ID.

**Response Body**

Field | Type | Description
------|------|------------
result | Object | [Experiment Resource](https://gdmsampletracking.docs.apiary.io/#reference/experiments)


