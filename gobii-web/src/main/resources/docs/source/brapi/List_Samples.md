
BrAPI term Sample corresponds to **Dna Sample** in GDM.

**Response Body** is a [BrAPI response](#brapilistresponsetemplate) with List of [Samples](#samplesresource) in result.data.


