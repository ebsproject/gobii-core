
BrAPI term CallSet corresponds to **DnaRun** in GDM.

**Response Body** is a [BrAPI response](#brapilistresponsetemplate) with List of [CallSets](#callsetresource) in result.data.


