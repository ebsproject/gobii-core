
List of all the Genotypes for a Search Query identified by searchResultDbId.

**Response Body** is a [BrAPI response](#brapilistresponsetemplate) with List of [Genotype Calls](#genotypecallsresource) in result.data.


