
List of all the Variants in a VariantSet(Dataset).

**Response Body** is a [BrAPI response](#brapilistresponsetemplate) with List of [Variants](#variantresource) in result.data.


