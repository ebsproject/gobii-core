
List of all the CallSets in a VariantSet(Dataset).

**Response Body** is a [BrAPI response](#brapilistresponsetemplate) with List of [CallSets](#callsetresource) in result.data.


