
BrAPI term Genome Map corresponds to **Mapset** in GDM.

**Response Body** is a [BrAPI response](#brapilistresponsetemplate) with List of [Genome Maps](#mapsresource) in result.data.


