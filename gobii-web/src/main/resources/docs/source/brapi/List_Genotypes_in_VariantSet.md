
List of all the Genotypes in a VariantSet(Dataset).

**Response Body** is a [BrAPI response](#brapilistresponsetemplate) with List of [Genotype Calls](#genotypecallsresource) in result.data.


