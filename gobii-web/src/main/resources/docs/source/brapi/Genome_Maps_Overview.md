
Brapi Genome Maps corresponds to Mapsets in GDM system.

<a name="mapsresource">**Resource Description**</a>

Field | Type | Description
------|------|------------
mapDbId | String | Id of genome map in GDM.
mapName | String | Name of the genome maps.
comments | String | Description of genome map.
type | String | Type of genome map.
linkageGroupCount | String | Number of linkage group in genome map.
markerCount | String | Number of markers in genome map. 

<a name="mapsresourceexample">**Resource Example**</a>

```

    {
        "mapDbId": "2",
        "mapName": "Maize_map",
        "type": "physical",
        "linkageGroupCount": 10,
        "markerCount": 80
    }

```


