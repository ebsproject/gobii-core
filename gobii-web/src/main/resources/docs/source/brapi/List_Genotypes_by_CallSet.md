
List of all the genotype calls in a given CallSet(DnaRun).

**Response Body** is a [BrAPI response](#brapilistresponsetemplate) with List of [GenotypeCalls](#genotypecallsresource) in result.data.


