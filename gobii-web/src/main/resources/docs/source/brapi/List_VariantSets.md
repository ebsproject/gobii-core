
BrAPI term VariantSet corresponds to **Dataset** in GDM.

**Response Body** is a [BrAPI response](#brapilistresponsetemplate) with List of [VariantSets](#variantsetresource) in result.data.


