
BrAPI term Studies corresponds to **Experiment** in GDM.

**Response Body** is a [BrAPI response](#brapilistresponsetemplate) with List of [Studies](#studiesresource) in *result.data*.


