
List of all the genotype calls in a given Variant(Marker/SNP).

**Response Body** is a [BrAPI response](#brapilistresponsetemplate) with List of [GenotypeCalls](#genotypecallsresource) in result.data.


