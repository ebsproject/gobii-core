
Brapi Studies corresponds to Experiment in GDM system.

<a name="studiesresource">**Resource Description**</a>

Field | Type | Description
------|------|------------
studyDbId | String | GDM DB Id for experiment
studyName | String | Name of the experiment.
contactDbId | String | Db Id of the Contact to which Experiment belongs.
email | String | Email Id of the contact to which experiment belongs.

<a name="studiesresourceexample">**Resource Example**</a>

```json
    
    {
        "studyDbId": "2",
        "studyName": "foo study",
        "contactDbId": "3",
        "email": "example@***",
        "studyCode": "bar code"
    }

```


