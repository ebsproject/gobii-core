
BrAPI term Variant corresponds to **Marker** in GDM.

**Response Body** is a [BrAPI response](#brapilistresponsetemplate) with List of [Variants](#variantresource) in result.data.


