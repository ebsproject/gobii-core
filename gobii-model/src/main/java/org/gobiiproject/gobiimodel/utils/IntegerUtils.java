package org.gobiiproject.gobiimodel.utils;

public class IntegerUtils {

    public static boolean isNullOrZero(Integer intToBeChecked) {
        return (intToBeChecked == null || intToBeChecked == 0);
    }
}
