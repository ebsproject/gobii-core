package org.gobiiproject.gobiimodel.config;

/**
 * Contains role values for CropAuth annotation usage.
 */
public class Roles {
    public static final String PI = "pi";
    public static final String ADMIN = "admin";
    public static final String CURATOR = "curator";
    public static final String USER = "user";
    
}