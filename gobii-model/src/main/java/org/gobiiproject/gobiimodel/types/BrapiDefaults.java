package org.gobiiproject.gobiimodel.types;

public class BrapiDefaults {

    public static final String pageSize = "1000";
    public static final String pageNum = "0";
    public static final String genotypesPageSize = "100000";


}
