package org.gobiiproject.gobiimodel.dto.noaudit;

public class SearchResultDTO {

    private String searchResultDbId;

    public String getSearchResultDbId() {
        return searchResultDbId;
    }

    public void setSearchResultDbId(String searchResultDbId) {
        this.searchResultDbId = searchResultDbId;
    }
}
