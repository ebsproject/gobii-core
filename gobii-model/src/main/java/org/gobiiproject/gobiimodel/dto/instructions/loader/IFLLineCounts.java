package org.gobiiproject.gobiimodel.dto.instructions.loader;

import lombok.Data;

public class IFLLineCounts {
    public int loadedData, existingData, invalidData;
}
