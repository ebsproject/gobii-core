package org.gobiiproject.gobiibrapi.calls.markerprofiles.allelematrices;

import org.gobiiproject.gobiibrapi.core.responsemodel.BrapiResponseDataList;

/**
 * Created by Phil on 12/18/2016.
 */
public class BrapiResponseAlleleMatrices extends BrapiResponseDataList<BrapiResponseAlleleMatricesItem> {


    public BrapiResponseAlleleMatrices() {}

}
