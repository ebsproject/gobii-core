package org.gobiiproject.gobiiprocess.digester.utils.validation;

@SuppressWarnings("serial")
public class MaximumErrorsValidationException extends Exception {
    MaximumErrorsValidationException() {
        super("Maximum exceptions raised.");
    }
}
