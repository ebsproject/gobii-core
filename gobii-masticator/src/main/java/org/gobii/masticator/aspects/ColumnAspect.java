package org.gobii.masticator.aspects;

public class ColumnAspect extends CoordinateAspect {
	public ColumnAspect(Integer row, Integer col) {
		super(row, col);
	}
}
