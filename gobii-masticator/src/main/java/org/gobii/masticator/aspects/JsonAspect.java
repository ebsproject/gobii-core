package org.gobii.masticator.aspects;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class JsonAspect extends ElementAspect{

	private TableAspect aspect;

}
