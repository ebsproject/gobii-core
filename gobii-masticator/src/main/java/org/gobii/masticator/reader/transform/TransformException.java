package org.gobii.masticator.reader.transform;

public class TransformException extends RuntimeException {

	public TransformException(Exception e) {
		super(e);
	}
}
